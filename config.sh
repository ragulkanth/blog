#!/bin/sh
sed -i "s%ABOUT_PAGE%${ABOUT_PAGE}%g" config.toml

# Generating the static files using hugo when -b arg is passed
if [ "$1" == "-b" ]
then
	echo "Generating the static files..."
	hugo
fi

