+++
author = "ragulkanth"
title = "Installing Arch Linux in an encrypted medium (USB 3.0 Pendrive)"
date = "2017-10-01"
description = "A step by step guide on installing Arch linux in an encrypted usb storage"
tags = [
    "arch linux",
    "encryption",
    "cryptography",
    "security",
    "installation",
    "linux",
]
categories = [
    "arch linux",
    "encryption",
    "cryptography",
    "security",
    "installation",
    "linux",
]
+++

Here we are gonna see how to install Arch Linux in an encrypted medium. For this blog I'm gonna use an USB 3.0 32GB Pendrive. This blog is also applicable for the native HDD or SSD installations too. So now lets discuss about some why's.

**Why encryption?**
There are two usuall reasons for it. The first is that you don't want others to mess up with your personal data basically your privacy and the second is the confidentiallity of the data (CLASSIFIED/TOP-SECRET). If you want to be more protected or really protected give a password of minimum length of about 10-20 characters which should contains alphabets (both small case and upper case), numbers and some specail characters. Try to be more random.

**Why in a pendrive?**
The simple answer is its portability and the other answer is a well personalized and secured operating system in your pocket or wallet. Use any computer hardware in your ownway. Without encryption don't go for a pendrive because easily we may miss it. From my personal experience USB 2.0 is not that fast. So go for USB 3.0.

**To whom this article is for?**
For the intermediate and advanced GNU/Linux user.

**Lets Begin the installation:**
Before proceeding to the installation note that you will lost your data please do a backup.

**Step 1:**
First you have to download the ArchLinux live iso from [here][download-arch] and write into a pendrive or CD/DVD. Here my choice is Pendrive.

For writing the iso in the pendrive(sdc) do the following in the terminal.

``` shell
sudo dd if=~/Downloads/arch-linux.iso of=/dev/sdc status=progress
```

Once it is done you are ready to go. Reboot your system and boot from your pendrive.

You are automatically logged in as root.

**Step 2:**
Here the actual Installation begins. Make sure that you are connected to the internet. First you have to set the date and time in your live system.

``` shell
timedatectl set-ntp true
```

**Step 3:**
Partitioning your pendrive for the installation. I'm going to use DOS partitioning table.

Here I'm gonna use only two partitions.

1. Boot partition (Mount point - /boot).
2. Root Partition (Mount point - /) which is going to be encrypted.

You may use any tool that you prefer for partitioning. I'm using **fdisk**.

To find your drive name do the following
``` shell
# this will list the drives connected to your system My pendrive is at /dev/sdc
lsblk

NAME            MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda               8:0    0 465.8G  0 disk  
├─sda1            8:1    0   200M  0 part  
├─sda2            8:2    0     1G  0 part  
└─sda3            8:3    0 464.6G  0 part  
  ├─fedora-root 254:1    0    50G  0 lvm   
  ├─fedora-home 254:2    0 410.7G  0 lvm   
  └─fedora-swap 254:3    0   3.9G  0 lvm   
sdc               8:16   1  28.9G  0 disk  
├─sdc1            8:17   1  28.9G  0 part

#To Partition
fdisk /dev/sdc

Welcome to fdisk (util-linux 2.31).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

# enter o to create DOS Partitioning table
Command (m for help): o

# Enter n to create new partion (/boot)
Command (m for help): n
# Now press p and then enter
# Now press 1 and then enter
# Now just press enter
# Now type +512M and the enter
# You are done in creating a partition for /boot

# Enter n to create new partion (/)
Command (m for help): n
# Now press e and then enter
# Now press 2 and then enter
# Now just press enter 
# Again enter
# You are done in creating a partition for /

#Set the boot flag for the /boot partition 
Command (m for help): a
# Now press 1 and then enter
```

So we are done with the partitioning.

**Step 3:**
Lets encrypt the root partition

``` shell
cryptsetup luksFormat /dev/sdc2
```
This will prompt for the password to enrypt. Don't forget the thing that said in the begining, a strong password.

The encryption is done here. In order to use the use this partition you have to decrypt.

``` shell
cryptsetup luksOpen /dev/sdc2 ROOT
```

This will open the partion to use and create the device mapping at /dev/mapper/ROOT.

**Step 3:**
Creating the file systems for both / and /boot

``` shell
#for the /boot partition
mkfs.ext4 /dev/sdc1

#for the / partition
mkfs.ext4 /dev/mapper/ROOT
```

**Step 4:**
Mounting the partitions. For working with the partitions we have to mount it in an appropriate folder.
 
``` shell
mount /dev/mapper/ROOT /mnt
#create a folder boot to mount the boot partition
mkdir /mnt/boot
mount /dev/sdc1 /mnt/boot
```

**Step 5:**
Installing the base system

``` shell
pacstrap /mnt base base-devel vim grub
```

This will install the basic system and also some extra package vim and grub.

**Step 6:**
Now we have to update the Filesystem Table where you have  partitions and its mount point.

``` shell
genfstab -U /mnt >> /mnt/etc/fstab
# here U stands for UUID
```

**Step 7:**
Login(chroot) to the new system that we have created.

``` shell
arch-chroot /mnt
```

**Step 8:**
Setting up the timezone, the language, the keyboard layout and the hostname.

**Timezone**
``` shell
#to list the timezones
ls /usr/share/zoneinfo/
#here my timezone is Asia/Kolkata for that
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
```

**Language**
``` shell
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
```

**Keyboard Layout**
``` shell
#replace us with your keyboard layout
echo "KEYMAP=us" > /etc/vconsole.conf
```

**Hostname**
Replace armour with your favourite name
``` shell
echo "armour" > /etc/hostname
```
Add the below line to the file /etc/hosts. I'm using **vim** for editing files.
``` shell
127.0.0.1	armour.armour	armour
```

Thats all you are done with your local settings.

**Step 9:**
We have to start some modules before booting to the Operating System in order to decrypt the encrypted ROOT partition. For that you have to edit the file **/etc/mkinitcpio.conf**.

In **HOOKS** add **keyboard** before **block** and add **encrypt** after **block**. The HOOKS may looks like below

``` shell
HOOKS=(base udev autodetect modconf keyboard block encrypt filesystems fsck)
```

Add **xhci-hcd** to MODULES.

``` shell
MODULES=(xhci-hcd)
``` 

Save and close the file. Now you have to generate the images for booting. For that do the following.

``` shell
mkinitcpio -p linux
```

**Step 10:**
Installing GRUB and setting up to decrypt the drive at the startup.

``` shell
grub-install --target=i386-pc /dev/sdc
```

Edit the file /etc/default/grub. Replace xxxxxxxxxx with the UUID of the encrypted partition.

``` shell
GRUB_CMDLINE_LINUX="cryptdevice=UUID=xxxxxxxxxx:ROOT earlymodules=xhci-hcd modules=xhci-hcd"
```

To get the UUID of the encrypted partition

``` shell
blkid /dev/sdc2
```

Now have to make the configuration of the grub.

``` shell
grub-mkconfig -o /boot/grub/grub.cfg
```

Almost done with the installation but we need more to make this Graphical Friendly.

**Step 11:**
Udating the root password
``` shell
passwd
```

Creating the New User.

``` shell
# replace xxx with your desired username
useradd -G wheel -md /home/xxx xxx
# set the password for the user
passwd xxx
```

Edit the visudo file for that do the following.

``` shell
#giving permission for the user group to execute all the commands
visudo
```

Here uncoment **%wheel ALL=ALL(ALL)** and save.

**Step 12:**
Installing the softwares that are needed to make this backbone more functional.

**Touchpad drivers:**

``` shell
pacman -S xf86-input-synaptics
```

**Video Drivers:**

``` shell
pacman -S xf86-video-vesa xf86-video-ati xf86-video-intel xf86-video-nouveau 
```

**Desktop Environments:**
Choose your desktop environments from [here][desktop-environments]. I have chosen **lxde**. For installing the other desktop environments go through the ArchWiki.

To install LXDE
``` shell
pacman -S lxde
```

To start lxde at the startup

``` shell
systemctl enable lxdm
```

We have to add dhcp at the startup too.

``` shell
systemctl enable dhcpcd
```

**Step 13:**

Installing other basic needed softwares (for me). This list may vary up to you.

``` shell
pacman -Sy chromium w3m terminator scite zim sylpheed openvpn dia keepassx2 profanity git python2-setuptools sudo python-setuptools python2-virtualenv firefox tigervnc openssh libreoffice wpa_supplicant wifi-menu dialog python-virtualenv gimp inkscape
```

Hurrah! We are done with the Installation. Just give a reboot.




[download-arch]: https://archlinux.org/download
[desktop-environments]: https://wiki.archlinux.org/index.php/desktop_environment
